import 'dart:convert';
import 'package:http/http.dart' as http_client;
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _switchLed = true;
  double _currentBrightnessValue = 25;
  double _currentTemperatureValue = 25;
  // double _currentSliderValue = 25;
  final String baseUrl = 'http://192.168.15.8:5000';
  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    _getStatusDeviceEvent();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(widget.title),
        ),
        elevation: 0,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: SizedBox(
              width: 150.0,
              height: 150.0,
              child: FloatingActionButton(
                elevation: 0,
                child: Text(
                  _switchLed ? 'ON' : 'OFF',
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 24.0,
                  ),
                ),
                onPressed: () async {
                  // _switchLed = !_switchLed;
                  await _turnOnOffPressed(context);
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(45.0),
            child: Column(
              children: [
                const Text(
                  'Brightness',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Slider(
                  value: _currentBrightnessValue,
                  min: 25,
                  max: 255,
                  divisions: 100,
                  label: _currentBrightnessValue.round().toString(),
                  onChanged: (double value) {
                    setState(() {
                      _currentBrightnessValue = value;
                    });
                  },
                  onChangeEnd: (double value) async {
                    await _changeStatusEvent();
                  },
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(45.0),
            child: Column(
              children: [
                const Text(
                  'Temperature',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Slider(
                  value: _currentTemperatureValue,
                  min: 0,
                  max: 255,
                  divisions: 100,
                  label: _currentTemperatureValue.round().toString(),
                  onChanged: (double value) {
                    setState(() {
                      _currentTemperatureValue = value;
                    });
                  },
                  onChangeEnd: (double value) async {
                    await _changeStatusEvent();
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  /// Functions events.
  Future<void> _changeStatusEvent() async {
    try {
      await _changeStatus();
      setState(() {});
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 2),
          content: Text(e.toString().split(':')[1]),
          action: SnackBarAction(
            label: 'Close',
            onPressed: () {
              // Code to execute.
            },
          ),
        ),
      );
    }
  }

  Future<void> _changeTemperatureEvent() async {
    try {
      await _changeTemperature(_currentTemperatureValue);
      setState(() {});
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 2),
          content: Text(e.toString().split(':')[1]),
          action: SnackBarAction(
            label: 'Close',
            onPressed: () {
              // Code to execute.
            },
          ),
        ),
      );
    }
  }

  Future<void> _turnOnOffPressed(BuildContext context) async {
    try {
      await _turnOnOffLigth();
      setState(() {});
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 2),
          content: Text(e.toString().split(':')[1]),
          action: SnackBarAction(
            label: 'Close',
            onPressed: () {
              // Code to execute.
            },
          ),
        ),
      );
    }
  }

  double reciprocal(double d) => 1 / d;

  Future<void> _getStatusDeviceEvent() async {
    try {
      List status = await _getStatusDevice();
      debugPrint('status: $status');

      _switchLed = status
          .where((element) => element['code'] == 'switch_led')
          .last['value'] as bool;
      _currentBrightnessValue = status
          .where((element) => element['code'] == 'bright_value')
          .last['value']
          .toDouble();
      _currentTemperatureValue = status
          .where((element) => element['code'] == 'temp_value')
          .last['value']
          .toDouble();
      setState(() {});
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(seconds: 2),
          content: Text(e.toString().split(':')[1]),
          action: SnackBarAction(
            label: 'Close',
            onPressed: () {
              // Code to execute.
            },
          ),
        ),
      );
    }
  }

  /// Functions services http.
  Future<bool> _turnOnOffLigth() async {
    _switchLed = !_switchLed;
    debugPrint("Turn ${_switchLed ? 'on' : 'off'}");
    var uri = "$baseUrl/turn-on-off?action=${_switchLed ? 'on' : 'off'}";

    debugPrint('URL: $uri');

    final response = await http_client.get(
      Uri.parse(uri),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      dynamic result = json.decode(response.body);
      if (result['data']) return result['data'];
      throw Exception(result['message']);
    } else {
      dynamic result = json.decode(response.body);
      throw Exception(result['message']);
    }
  }

  Future<bool> _changeBrightness(double brightness) async {
    var uri = "$baseUrl/brightness";

    debugPrint('URL: $uri');

    final response = await http_client.post(
      Uri.parse(uri),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        {"brightness": brightness},
      ),
    );

    if (response.statusCode == 200) {
      dynamic result = json.decode(response.body);
      if (result['data']) return result['data'];
      throw Exception(result['message']);
    } else {
      dynamic result = json.decode(response.body);
      throw Exception(result['message']);
    }
  }

  Future<bool> _changeTemperature(double temperature) async {
    var uri = "$baseUrl/temperature";

    final response = await http_client.post(
      Uri.parse(uri),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        {"temperature": temperature},
      ),
    );

    if (response.statusCode == 200) {
      dynamic result = json.decode(response.body);
      if (result['data']) return result['data'];
      throw Exception(result['message']);
    } else {
      dynamic result = json.decode(response.body);
      throw Exception(result['message']);
    }
  }

  Future<dynamic> _getStatusDevice() async {
    var uri = "$baseUrl/status";

    final response = await http_client.get(
      Uri.parse(uri),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      dynamic result = json.decode(response.body);
      if (result['data'] != null) return result['data'];
      throw Exception(result['message']);
    } else {
      dynamic result = json.decode(response.body);
      throw Exception(result['message']);
    }
  }

  Future<dynamic> _changeStatus() async {
    var uri = "$baseUrl/changeStatus";

    final response = await http_client.post(
      Uri.parse(uri),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        {
          'commands': [
            {'code': 'switch_led', 'value': !_switchLed},
            {'code': 'bright_value', 'value': _currentBrightnessValue.toInt()},
            {'code': 'temp_value', 'value': _currentTemperatureValue.toInt()},
          ]
        },
      ),
    );

    if (response.statusCode == 200) {
      dynamic result = json.decode(response.body);
      if (result['data'] != null) return result['data'];
      throw Exception(result['message']);
    } else {
      dynamic result = json.decode(response.body);
      throw Exception(result['message']);
    }
  }
}
